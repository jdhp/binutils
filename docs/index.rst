========
binutils
========

Python Binary Tools

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#binutils
* Online documentation: https://jdhp.gitlab.io/binutils
* Source code: https://gitlab.com/jdhp/binutils
* Issue tracker: https://gitlab.com/jdhp/binutils/issues
* binutils on PyPI: https://pypi.org/project/binutils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

